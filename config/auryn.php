<?php

/** @var $injector */

/**
 * Si no hay ninguna implementación definida, usaremos la del ayuntamiento.
 */
if (getenv('EVENT_CALENDAR_IMPLEMENTATION') === 'ayto' or getenv('EVENT_CALENDAR_IMPLEMENTATION') === false) {
    require_once __DIR__ . '/../../../../funciones.php';
    $injector->alias(
        '\Bittacora\EventsCalendar\Domain\EventRepository',
        '\Bittacora\EventsCalendar\Infrastructure\Ayto\AytoEventRepository'
    );

    $injector->alias(
        '\Bittacora\EventsCalendar\Domain\CategoryRepository',
        '\Bittacora\EventsCalendar\Infrastructure\Ayto\AytoCategoryRepository'
    );
}
