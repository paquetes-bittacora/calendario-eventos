<?php

namespace Bittacora\EventsCalendar\Domain\Exceptions;

class InvalidDateRangeException extends \Exception
{
    protected $message = 'El rango de fechas no es válido. Compruebe que la fecha de fin no sea anterior
                          a la de inicio.';
}
