<?php

namespace Bittacora\EventsCalendar\Domain;

use DateTime;

interface EventRepository
{
    /**
     * Devuelve los eventos agrupados por día
     * @param DateTime $startDateTime
     * @param DateTime $endDateTime
     * @param array $categories Categorías que se deben incluir (opcional)
     * @return mixed
     */
    public function getEventsBetweenDates(DateTime $startDateTime, DateTime $endDateTime, $category = null);
}
