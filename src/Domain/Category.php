<?php

namespace Bittacora\EventsCalendar\Domain;

use JsonSerializable;

class Category implements JsonSerializable
{
    private $id;

    private $name;
    
    private $children;

    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'children' => $this->getChildren()
        ];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    public function addChildren(Category $child)
    {
        $this->children[] = $child;
    }
}
