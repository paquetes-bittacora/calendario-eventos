<?php


namespace Bittacora\EventsCalendar\Domain;


use DateTime;

class DateConverter
{

    public static function dateTimeFromMysql($date, $time = null)
    {
        $format = 'Y-m-d';

        if (empty($date) || $date === '0000-00-00') {
            return null;
        }

        if ($time !== null) {
            $format .= ' H:i:s';
            $date .= ' ' . $time;
        }

        return DateTime::createFromFormat($format, $date);
    }
}
