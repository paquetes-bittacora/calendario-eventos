<?php

namespace Bittacora\EventsCalendar\Domain;

use Bittacora\EventsCalendar\Domain\Exceptions\InvalidDateRangeException;
use DateTime;
use JsonSerializable;

class Event implements JsonSerializable
{
    private $id;
    /** @var string */
    private $description;
    /** @var DateTime|null */
    private $endDate;
    /** @var array */
    private $htmlAttributes;
    /** @var string */
    private $location;
    /** @var string */
    private $name;
    /** @var DateTime */
    private $startDate;
    /** @var Category */
    private $category;
    /** @var string */
    private $url;

    /**
     * Event constructor.
     * @param string $name Nombre del evento
     * @param DateTime $startDate Fecha de inicio (obligatoria)
     * @param DateTime|null $endDate Fecha de finalización (opcional)
     * @param array $htmlAttributes Atributos que se usarán a la hora de representar el calendario en el template (array
     *                             libre).
     * @throws InvalidDateRangeException
     */
    public function __construct($id, $name, $startDate, $endDate = null, $htmlAttributes = [])
    {
        $this->checkDates($startDate, $endDate);

        $this->name = $name;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->htmlAttributes = $htmlAttributes;
        $this->id = $id;
    }

    /**
     * @throws InvalidDateRangeException Si la fecha de fin es anterior a la de inicio.
     */
    private function checkDates($startDate, $endDate)
    {
        if ($endDate !== null && $startDate->diff($endDate)->invert === 1 && $startDate->diff($endDate)->days !== 0) {
            throw new InvalidDateRangeException();
        }
    }

    public function jsonSerialize()
    {
        $startDateFormat = 'd M Y \a \l\a\s H:i';
        if ($this->getStartDate()->format('H:i') === '00:00') {
            $startDateFormat = 'd M Y';
        }
        $startDate = $this->getStartDate()->format($startDateFormat);

        $endDate = $this->getEndDate();
        if ($endDate !== null) {
            $endDate = $endDate->format('d M Y');
        }

        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'startDate' => $startDate,
            'endDate' => $endDate,
            'htmlAttributes' => $this->getHtmlAttributes(),
            'description' => $this->getDescription(),
            'location' => $this->getLocation(),
            'category' => $this->getCategory(),
            'url' => $this->getUrl()
        ];
    }

    /**
     * @return DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @return DateTime|null
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getHtmlAttributes()
    {
        return $this->htmlAttributes;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param string $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

}
