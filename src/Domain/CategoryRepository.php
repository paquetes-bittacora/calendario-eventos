<?php

namespace Bittacora\EventsCalendar\Domain;

interface CategoryRepository
{
    /**
     * @param string $id
     * @return Category
     */
    public function getById($id);

    public function getAll();
}
