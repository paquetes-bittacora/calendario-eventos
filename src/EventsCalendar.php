<?php

namespace Bittacora\EventsCalendar;

use Bittacora\EventsCalendar\Domain\CategoryRepository;
use Bittacora\EventsCalendar\Domain\DateConverter;
use Bittacora\EventsCalendar\Domain\EventRepository;

class EventsCalendar
{
    public function getEvents($request, EventRepository $eventRepository)
    {
        $startDateTime = DateConverter::dateTimeFromMysql($request->query->get('startDate'));
        $endDateTime = DateConverter::dateTimeFromMysql($request->query->get('endDate'));

        $category = $request->query->get('category');

        return $eventRepository->getEventsBetweenDates($startDateTime, $endDateTime, $category);
    }

    public function getCategories($request, CategoryRepository $categoryRepository)
    {
        return $categoryRepository->getAll();
    }
}
