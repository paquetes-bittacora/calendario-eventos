<?php

namespace Bittacora\EventsCalendar\Infrastructure\Ayto;

use Bittacora\EventsCalendar\Domain\CategoryRepository;
use Bittacora\EventsCalendar\Domain\DateConverter;
use Bittacora\EventsCalendar\Domain\Event;
use Bittacora\EventsCalendar\Domain\EventRepository;
use Bittacora\EventsCalendar\Domain\Exceptions\InvalidDateRangeException;
use DateInterval;
use DatePeriod;
use DateTime;

class AytoEventRepository implements EventRepository
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function getEventsBetweenDates(DateTime $startDateTime, DateTime $endDateTime, $category = null)
    {
        $result = [];

        $startDate = $startDateTime->format('Y-m-d H:i:s');
        $endDate = $endDateTime->format('Y-m-d H:i:s');

        $rows = dbquery('SELECT * FROM bi_contenidos WHERE id_contenido!="0" AND publicado="1" 
                                   AND f_ini >= "' . $startDate . '" AND f_fin <= "' . $endDate . '"
                                   AND tipo = 20 ' . $this->getCategoriesFilter($category));

        while ($row = dbarray($rows)) {
            $result = $this->addEventToResult($row, $result);
        }

        return $result;
    }

    /**
     * @throws InvalidDateRangeException
     */
    private function eventFromRow($row)
    {
        $event = new Event(
            $row['id_contenido'],
            utf8_encode(html_entity_decode($row['titulo1_es'])),
            DateConverter::dateTimeFromMysql($row['f_ini'], $row['hora']),
            DateConverter::dateTimeFromMysql($row['f_fin']),
            []
        );
        $event->setDescription($row['txt1_es']);
        $event->setLocation($row['txt3_es']);

        $event->setCategory($this->categoryRepository->getById($row['categoria']));
        
        return $event;
    }

    private function getCategoriesFilter($category)
    {
        $filtrosubcat = '';

        if (empty($category) or $category == 'null') {
            return '';
        }
        
        $sub = dbarray(dbquery("SELECT * FROM bi_categorias WHERE tipo='20'  AND id='" . $category . "'"));

        if ($sub['ppal'] != 0) {
            $filtrosubcat = " AND categoria='" . $category . "'";
        } else {
            $rows = dbcount("(id)", "categorias", "tipo='20' AND ppal='" . $sub['id'] . "'");
            if ($rows != 0) {
                $selects_subcats = dbquery("SELECT * FROM bi_categorias WHERE tipo='20'  
                          AND ppal='" . $sub['id'] . "'");
                $filtrosubcat = " AND (categoria=" . $sub['id'] . " ";
                while ($datacat = dbarray($selects_subcats)) {
                    $filtrosubcat .= "OR categoria=" . $datacat['id'] . " ";
                }
                $filtrosubcat .= ")";
            } else {
                $filtrosubcat = " AND categoria='" . $category . "'";
            }
        }

        return $filtrosubcat;
    }

    /**
     * @param array $result
     * @param $eventStartDate
     * @param Event $event
     * @return array
     */
    private function addEventToDate(array $result, $eventStartDate, Event $event)
    {
        if (!isset($result[$eventStartDate])) {
            $result[$eventStartDate] = [];
        }
        $result[$eventStartDate][] = $event;
        return $result;
    }

    /**
     * @param array $row
     * @param $result
     * @return array|mixed
     * @throws InvalidDateRangeException
     */
    private function addEventToResult(array $row, $result)
    {
        $event = $this->eventFromRow($row);
        $eventStartDateTime = $event->getStartDate();
        $eventStartDate = $eventStartDateTime->format('Y-m-d');
        $eventEndDateTime = $event->getEndDate();

        if (!empty($eventEndDateTime)) {
            $period = new DatePeriod(
                $eventStartDateTime,
                new DateInterval('P1D'),
                $eventEndDateTime
            );
            if (!empty($period)) {
                foreach ($period as $date) {
                    $result = $this->addEventToDate($result, $date->format('Y-m-d'), $event);
                }
            }
        } else {
            $result = $this->addEventToDate($result, $eventStartDate, $event);
        }
        return $result;
    }
}
