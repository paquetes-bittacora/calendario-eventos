<?php

namespace Bittacora\EventsCalendar\Infrastructure\Ayto;

use Bittacora\EventsCalendar\Domain\Category;
use Bittacora\EventsCalendar\Domain\CategoryRepository;

class AytoCategoryRepository implements CategoryRepository
{

    public function getAll()
    {
        $result = [];
        $query = dbquery('SELECT id FROM bi_categorias WHERE tipo = 20 AND ppal = 0 AND id != 720');

        while ($row = dbarray($query)) {
            $mainCategory = $this->getById($row['id']);
            $childquery = dbquery('SELECT id FROM bi_categorias WHERE tipo = 20 AND ppal = "' . $row['id'] . '"');
            while ($childRow = dbarray($childquery)) {
                $mainCategory->addChildren($this->getById($childRow['id']));
            }
            $result[] = $mainCategory;
        }
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getById($id)
    {
        $row = dbarray(dbquery('SELECT * FROM bi_categorias WHERE id ="' . $id . '"'));
        return new Category($row['id'], utf8_encode($row['titulo_es']));
    }

}
