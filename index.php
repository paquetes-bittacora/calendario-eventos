<?php

use Bittacora\EventsCalendar\EventsCalendar;
use Symfony\Component\HttpFoundation\Request;

require_once __DIR__ . '/../../autoload.php';

$injector = new Auryn\Injector;

require_once __DIR__ . '/config/auryn.php';

$eventsCalendar = $injector->make('Bittacora\EventsCalendar\EventsCalendar');

$request = Request::createFromGlobals();
$injector->defineParam('request', $request);

echo json_encode($injector->execute([$eventsCalendar, $_GET['method']]));
