
Calendario de eventos

-----------------------------------

API para generar calendarios de eventos.

  

Compatible con PHP 5.4 porque se desarrolló inicialmente para el ayuntamiento. No lo he probado en otras versiones de PHP.

  

Instalación
-----------

Para instalar este paquete en un proyecto, una vez se descargue hay que incluir en el archivo .htaccess las siguientes reglas, adaptadas según el framework, para que el módulo pueda recibir peticiones:
  

`
RewriteRule ^calendario-eventos/(.*?)/?$ vendor/bittacora/calendario-eventos-api/index.php?method=$1 [L,QSA]
`

También se debe declarar una variable de entorno en el index.php (o configuracion.php, etc.) de la aplicación que indique la implementación que se quiere usar:

`
putenv('EVENT_CALENDAR_IMPLEMENTATION=ayto');
`

Implementaciones
-----
Se ha intentado desacoplar lo máximo posible el módulo para facilitar su reusabilidad. Las únicas interfaces que hay que implementar (si no hay una implementación disponible) son `Bittacora\EventsCalendar\Domain\CategoryRepository` y `Bittacora\EventsCalendar\Domain\EventRepository`. Se incluye una implementación basada en el bPanel del Ayuntamiento.

Para usar otra implementación, hay que registrarla en `config/auryn.php`, para que el inyector de dependencias ([rdlowrey/auryn](https://github.com/rdlowrey/auryn)) sepa qué clases implementan esas interfaces.
