<?php


class EventsCalendarCest
{
    public function _after(AcceptanceTester $I)
    {
    }

    public function _before(AcceptanceTester $I)
    {
        date_default_timezone_set('Europe/Madrid');
        $_SERVER['REQUEST_URI'] = '';
        require_once __DIR__ . '/../../../../funciones.php';
    }

    /**
     * Dado tenemos dos fechas en las que sabemos que hay eventos
     * al llamar a getEvents
     * devuelve un array de eventos
     */
    public function devuelveEventosEntreDosFechas(AcceptanceTester $I)
    {
        // Given
        $_GET['method'] = 'getEvents';
        $_GET['startDate'] = '2021-05-01';
        $_GET['endDate'] = '2021-05-31';

        // When
        $result = json_decode($this->getCallResult(), true);

        // Then
        $firstKey = $this->firstArrayKey($result);
        $I->assertTrue(is_array($result));
        $I->assertArrayHasKey('name', $result[$firstKey][0]);
        $I->assertArrayHasKey('startDate', $result[$firstKey][0]);
        $I->assertArrayHasKey('endDate', $result[$firstKey][0]);
        $I->assertArrayHasKey('htmlAttributes', $result[$firstKey][0]);
        $I->assertRegExp('/[\d]{4}-[\d]{2}-[\d]{2}/', $firstKey);
    }

    /**
     * Como la salida del módulo será un echo, tengo que capturarla para poder procesarla.
     * @return false|string
     */
    private function getCallResult()
    {
        ob_start();
        require __DIR__ . '/../../index.php';
        return ob_get_clean();
    }

    private function firstArrayKey($arr)
    {
        foreach ($arr as $key => $unused) {
            return $key;
        }
        return null;
    }


}
